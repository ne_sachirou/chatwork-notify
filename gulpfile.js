'use strict';
var gulp   = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs');

gulp.task('js-build', function () {
  return gulp.src(['src/bower_components/favico.js/favico.js', 'src/contentscript.js']).
    pipe(concat('contentscript.js')).
    pipe(uglify({compress : {unsafe : true}})).
    pipe(gulp.dest('chatwork-notify'));
});

gulp.task('watch', function () {
  gulp.watch(['src/*.js'], ['js-build']);
});

gulp.task('build'  , ['js-build']);
gulp.task('test'   , []);
gulp.task('default', []);
