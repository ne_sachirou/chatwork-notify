(function () {
'use strict';

var prevCount = 0,
    favicon   = new Favico();

function loop() {
  var count = 0,
      match = document.title.match(/^\[(\d+)\]/);
  if (match) {
    count = match[1] - 0;
  }
  if (count !== prevCount) {
    prevCount = count;
    favicon.badge(count);
  }
}

window.setInterval(loop, 1000);
loop();

}());
